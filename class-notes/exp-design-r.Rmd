# Experimental Design in R

Grab packages

```{r, eval = FALSE}
library(tidyverse)
library(broom)
library(pwr)
library(simputation)
library(sampling)
library(agricolae)
```

Load data

ToothGrowth dataset in the base R

```{r, eval = FALSE}
# Load the ToothGrowth dataset
data(ToothGrowth)

ToothGrowth <- ToothGrowth %>% mutate(dose = as.factor(dose))

ToothGrowth %>% glimpse()
```

Sample of Lending Club data

```{r, eval = FALSE}
lendingclub <- read_csv("../data/lendclub_samp.csv", col_types = "iiifdffdffff") %>%
  mutate(grade = factor(grade, levels = sort(levels(lendingclub$grade))))

lendingclub %>% glimpse()
```


##  Introduction to Experimental Design

Steps of an experiment

  - Planning
    - dependent variable = outcome
    - independent variable(s) = explanatory variables
  - Design
  - Analysis

Key components of an experiment

  - Randomization : evenly distributes any variability in outcome due to outside factor across treatment groups
  - Replication : must repeat an experiment to fully assess variability
  - Blocking : Helps control variability by making treatment groups more alike.

### A basic experiment {-}

Let's dive into experimental design. Note that all of these concepts will be covered in more detail in the next video, "Hypothesis Testing."

ToothGrowth is a built-in R dataset from a study that examined the effect of three different doses of Vitamin C on the length of the odontoplasts, the cells responsible for teeth growth in 60 guinea pigs, where tooth length was the measured outcome variable.


If you wanted to conduct a two-sided t-test with the famous mtcars dataset, it would look like this, where x is the outcome in question, alternative is set to "two.sided", and mu is the value you're testing to see if the mean of mpg is not equal to.

```
data(mtcars)

t.test(x = mtcars$mpg, alternative = "two.sided", mu = 40)
```

Suppose you know that the average length of a guinea pigs odontoplasts is 18 micrometers. Conduct a two-sided t-test on the ToothGrowth dataset. Here, a two-sided t-test will check to see if the mean of len is not equal to 18.


```{r, eval = FALSE}
# Perform a two-sided t-test
t.test(x = ToothGrowth$len, alternative = "two.sided", mu = 18)
```

### Randomization {-}

Randomization of subjects in an experiment helps spread any variability that exists naturally between subjects evenly across groups. For `ToothGrowth`, an example of effective randomization would be randomly assigning male and female guinea pigs into different experimental groups, ideally cancelling out any existing differences that naturally exist between male and female guinea pigs.

In the experiment that yielded the `ToothGrowth` dataset, guinea pigs were randomized to receive Vitamin C either through orange juice or ascorbic acid, indicated in the dataset by the supp variable. It's natural to wonder if there is a difference in tooth length by supplement type - a question that a t-test can also answer!

Starting with this exercise, you should use `t.test()` and other modeling functions with formula notation:

```
t.test(outcome ~ explanatory_variable, data = dataset)
```

This can be read: "test outcome by explanatory_variable in my dataset." The default test for `t.test()` is a two-sided t-test.

You no longer have to explicitly declare `dataset$outcome`, because the data argument is specified.

```{r, eval = FALSE}
# Perform a t-test
ToothGrowth_ttest <- t.test(len ~ supp, data = ToothGrowth)

# Load broom
library(broom)

# Tidy ToothGrowth_ttest
ToothGrowth_ttest %>% tidy()
```

### Replication {-}

Recall that replication means you need to conduct an experiment with an adequate number of subjects to achieve an acceptable statistical power. Sample size and power will be discussed in more detail later in this chapter.


```{r, eval = FALSE}
# Count number of observations for each combination of supp and dose
ToothGrowth %>% 
    count(dose, supp)
```

### Blocking {-}

Though this is not true, suppose the supplement type is actually a nuisance factor we'd like to control for by blocking, and we're actually only interested in the effect of the dose of Vitamin C on guinea pig tooth growth.

If we block by supplement type, we create groups that are more similar, in that they'll have the same supplement type, allowing us to examine only the effect of dose on tooth length.

We'll use the `aov()` function to examine this. `aov()` creates a linear regression model by calling `lm()` and examining results with `anova()` all in one function call. To use `aov()`, we'll still need functional notation, as with the randomization exercise, but this time the formula should be `len ~ dose + supp` to indicate we've blocked by supplement type. (We'll cover `aov()` and `anova()` in more detail in the next chapter.)

```{r, eval = FALSE}
# Create a boxplot with geom_boxplot()
ggplot(ToothGrowth, aes(x = dose, y = len)) + 
    geom_boxplot()

# Create ToothGrowth_aov
ToothGrowth_aov <- aov(len ~ dose + supp, data = ToothGrowth)

# Examine ToothGrowth_aov with summary()
summary(ToothGrowth_aov)
```

### Hypothesis Testing {-}

   - Null hypothesis
    - There is no change
    - No difference between groups
    - The stat (mean, median, etc) == a number
  - Alternative hypothesis
    - there is a change
    - yes difference between groups
    - the stat (mean, median, etc) is >, <, or != to a number
      - `>`, `<` one-sided test
      - `!=` two-sided test
    
  *Power* : probability that the test correctly rejects the null hypothesis when the alternative hypothesis is true
  
  Rule of thumb: have 80% power for experiments, which you need adequate sample size
  
  *Effect Size* standardized measure of the difference you're trying to detect
  
  *Sample Size* How many experimental units you need to survey the detect the desired difference at the desired power.

### One sided vs. Two-sided tests {-}

Recall in the first exercise that we tested to see if the mean of the guinea pigs' teeth in ToothGrowth was not equal to 18 micrometers. That was an example of a two-sided t-test: we wanted to see if the mean of len is some other number on either side of 18.

We can also conduct a one-sided t-test, explicitly checking to see if the mean is less than or greater than 18. Whether to use a one- or two-sided test usually follows from your research question. Does an intervention cause longer tooth growth? One-sided, greater than. Does a drug cause the test group to lose more weight? One-sided, less than. Is there a difference in mean test scores between two groups of students? Two-sided test.

```{r, eval = FALSE}
# Less than
t.test(
  x = ToothGrowth$len,
  alternative = "less",
  mu = 18
)

# Greater than
t.test(
  x = ToothGrowth$len,
  mu = 18,
  alternative = "greater"
)
```

### Power & Sample Size Calculations {-}

One key part of designing an experiment is knowing the required sample size you'll need to be able to test your hypothesis.

The pwr package provides a handy function, `pwr.t.test()`, which will calculate that for you. However, you do need to know your desired significance level (often 0.05), if the test is one- or two-sided, if the data is from one sample, two samples, or paired, the effect size, and the power. Some of this information will be given to you or can be reasoned from the design.

A power or sample size calculation is usually different each time you conduct one, and the details of the calculation strongly depend on what kind of experiment you're designing and what your end goals are.

```{r, eval = FALSE}
# Calculate power
pwr.t.test(
  n = 100, # number of obs (per sample)
  d = .35, # effect size (Cohen's d)
  sig.level = 0.10,
  type = "two.sample", 
  alternative = "two.sided",
  power = NULL
)

# Calculate sample size
pwr.t.test(
  n = NULL,
  d = 0.2, # effect size
  sig.level = 0.05,
  type = "two.sample",
  alternative = "two.sided",
  power = 0.8
)
```


## Basic Experiments

### Single & Multiple Factor Experiments {-}

ANOVA - Used to compare 3+ groups

- An omnibus test:
  - won't know which group's mean is different without additional post hoc testing

### Exploratory Data Analysis (EDA) Lending Club {-}

```{r, eval = FALSE}
lendingclub %>% glimpse()


# Find median loan_amnt and mean int_rate, annual_inc with summarize()
lendingclub %>%
  summarize(
    median(loan_amnt),
    mean(int_rate),
    mean(annual_inc)
  )

# Use ggplot2 to build a bar chart of purpose
lendingclub %>%
  ggplot(aes(x = purpose)) +
  geom_bar() +
	coord_flip()
```


```{r, eval = FALSE}

purpose_recode_ls = c(
  "credit_card" = "debt_related", 
  "debt_consolidation" = "debt_related",
  "medical" = "debt_related",
  "car" = "big_purchase", 
  "major_purchase" = "big_purchase", 
  "vacation" = "big_purchase",
  "moving" = "life_change", 
  "small_business" = "life_change", 
  "wedding" = "life_change",
  "house" = "home_related", 
  "home_improvement" = "home_related"
)


lendingclub <- lendingclub %>%
  mutate(purpose_recode = recode(purpose, !!!purpose_recode_ls))

lendingclub %>% head(n=10)
```

How does loan purpose affect amount funded?

In the last exercise, we pared the `purpose` variable down to a more reasonable 4 categories and called it `purpose_recode`. As a data scientist at Lending Club, we might want to design an experiment where we examine how the loan purpose influences the amount funded, which is the money actually issued to the applicant.

Remember that for an ANOVA test, the null hypothesis will be that all of the mean funded amounts are equal across the levels of `purpose_recode.` The alternative hypothesis is that at least one level of `purpose_recode` has a different mean. We will not be sure which, however, without some post hoc analysis, so it will be helpful to know how ANOVA results get stored as an object in R.


```{r, eval = FALSE}
# Build a linear regression model, purpose_recode_model
purpose_recode_model <- lm(funded_amnt ~ purpose_recode, data = lendingclub)

# Examine results of purpose_recode_model
summary(purpose_recode_model)

# Get anova results and save as purpose_recode_anova
purpose_recode_anova <- anova(purpose_recode_model)

# Print purpose_recode_anova
purpose_recode_anova

# Examine class of purpose_recode_anova
class(purpose_recode_anova)
```

Before we examine other factors besides purpose_recode that might influence the amount of loan funded, let's examine which means of purpose_recode are different. This is the post-hoc test referred to in the last exercise.

The result of that ANOVA test was statistically significant with a very low p-value. This means we can reject the null hypothesis and accept the alternative hypothesis that at least one mean was different. But which one?

We should use Tukey's HSD test, which stands for Honest Significant Difference. To conduct Tukey's HSD test in R, you can use `TukeyHSD()`:

```
TukeyHSD(aov_model, "outcome_variable_name", conf.level = 0.9)
```

This would conduct Tukey's HSD test on some aov_model, looking at a specific "outcome_variable_name", with a conf.level of 90%.

```{r, eval = FALSE}
# Use aov() to build purpose_aov
purpose_aov <- aov(funded_amnt ~ purpose_recode, data = lendingclub)

# Conduct Tukey's HSD test to create tukey_output
tukey_output <- TukeyHSD(purpose_aov,  which = "purpose_recode", conf.level = 0.95)

# Tidy tukey_output to make sense of the results
tukey_output %>% broom::tidy()
```

### Multiple Factor Experiments {-}

We tested whether the purpose of a loan affects loan amount funded and found that it does. However, we also know that it's unlikely that loans are funded based only on their intended purpose. It's more likely that the company is looking at a holistic picture of an applicant before they decide to issue a loan.

We can examine more than one explanatory factor in a multiple factor experiment. Like our experiments on ToothGrowth from Chapter 1, an experimenter can try and control two (or more!) different factors and see how they affect the outcome. We're using open data, so we can't quite control the factors here (they're submitted as someone fills out their loan application), but let's look at how a few other factors affect loan amount funded.

```{r, eval = FALSE}
# Use aov() to build purpose_emp_aov
purpose_emp_aov <- aov(funded_amnt ~ emp_length + purpose_recode, data = lendingclub)

# Print purpose_emp_aov to the console
purpose_emp_aov

# Call summary() to see the p-values
summary(purpose_emp_aov)
```

### Model Validation {-}

Post-modeling validation (for anova)

  - Residual plot
  - QQ-plot for normality
  - Residual vs leverage (which levels does the model fit best?)
  - Test ANOVA assumptions
    - Homogeneity of variances
  - Try non-parametric alterntaives to ANOVA
  
Let's do some EDA with our experiment in mind. Lending Club has now asked you, their data scientist, to examine what effect their Lending Club-assigned loan `grade` variable has on the interest rate, `int_rate.` They're interested to see if the grade they assign the applicant during the process of applying for the loan affects the interest rate ultimately assigned to the applicant during the repayment process.

```{r, eval = FALSE}
# Examine the summary of int_rate
summary(lendingclub$int_rate)

# Examine int_rate by grade
lendingclub %>% 
	group_by(grade) %>% 
	summarize(
	  mean = mean(int_rate), 
	  var = var(int_rate), 
	  median = median(int_rate)
  )

# Make a boxplot of int_rate by grade
lendingclub %>%
  ggplot(aes(x = grade, y = int_rate)) +
  geom_boxplot()

# Use aov() to create grade_aov plus call summary() to print results
grade_aov <- aov(int_rate ~ grade, data = lendingclub)
grade_aov %>% summary()
```

### Post-modeling validation plots and variance {-}

In the last exercise, we found that `int_rate` does differ by `grade.` Now we should validate this model, which for linear regression means examining the Residuals vs. Fitted and Normal Q-Q plots.

If you call `plot()` on a model object in R, it will automatically plot both of those plots plus two more. You'll interpret these plots to evaluate model fit. We discussed how to do this in the video.

Another assumption of ANOVA and linear modeling is homogeneity of variance. Homogeneity means "same", and here that would mean that the variance of `int_rate` is the same for each level of grade. We can test for homogeneity of variances using `bartlett.test()`, which takes a formula and a dataset as inputs.

```{r, eval = FALSE}
# For a 2x2 grid of plots:
par(mfrow=c(2, 2))

# Plot grade_aov
plot(grade_aov)

# Bartlett's test for homogeneity of variance
bartlett.test(int_rate ~ grade, data = lendingclub)
```

### Kruskal-Wallis rank sum test {-}

Given that we found in the last exercise that the homogeneity of variance assumption of linear modeling was violated, we may want to try an alternative.

One non-parametric alternative to ANOVA is the Kruskal-Wallis rank sum test. For those with some statistics knowledge, it is an extension of the Mann-Whitney U test for when there are more than two groups, like with our grade variable. For us, the null hypothesis for this test would be that all of the `int_rates` have the same ranking by `grade.`

The Kruskal-Wallis rank sum test can be conducted using the `kruskal.test()` function, available in base R. Luckily for you, the use of this function is very similar to using `lm()` or `aov()`: you input a formula and a dataset, and a result is returned.

```{r, eval = FALSE}
kruskal.test(
  int_rate ~ grade,
  data = lendingclub
)
```

### A/B Testing {- }

We know now that we need to analyze our A/B test results with a t-test after we've collected data. We have two pretty important questions we need to answer before we do that: what's the effect size and what's the sample size required for this test?

In this case, effect size was given to us. Lending Club is looking to detect the relatively small effect size of 0.2. We'll again use the pwr package and calculate sample size using an appropriate function to find out how many we'll need to recruit into each group, A and B.

```{r, eval = FALSE}
pwr.t.test(
  d = 0.2, # effect size
  power = 0.8,
  sig.level = 0.05,
  type = "two.sample",
  alternative = "less",
  n = NULL
)
```

Now that we know the sample size required, and we allowed the experiment to run long enough to get at least 400 people in each group, we can analyze our A/B test.

Remember that when applicants were using the Lending Club website, they were randomly assigned to two groups, A or B, where A was shown a mint green website header and B was shown a light blue website header. Lending Club was interested to see if website header color choice influenced `loan_amnt`, the amount an applicant asked to borrow.

A new dataset, `lendingclub_ab` is available in your workspace. The A/B test was run until there were 500 applicants in each group. Each applicant has been labeled as group A or B. Conduct the proper test to see if the mean of loan_amnt is different between the two groups.

I DO NOT HAVE THE `lendingclub_ab` dataset. CANNOT RUN CODE!!!!

```{r, eval = FALSE}
# Plot the A/B test results
ggplot(lendingclub_ab, aes(y = loan_amnt, x = Group)) +
  geom_boxplot()

# Conduct a two-sided t-test
t.test(loan_amnt ~ Group, data = lendingclub_ab)
```

The point of an A/B test is that only one thing is changed and the effect of that change is measured. We saw this with our examples in the video and the last few exercises. On the other hand, a multivariate experiment, such as the ToothGrowth experiment from chapter 1, is where a few things are changed (and is similar to a multiple factor experiment, which we covered earlier in this chapter.)

A Lending Club multivariate test can combine all of the explanatory variables we've looked at in this chapter. Let's examine how Group, grade, and verification_status affect loan_amnt in the lendingclub_ab dataset.

```{r, eval = FALSE}
# Build lendingclub_multi
lendingclub_multi <-lm(loan_amnt ~ grade + verification_status + Group, data = lendingclub_ab)

# Examine lendingclub_multi results
lendingclub_multi %>% tidy()
```

## Randomized Complete

Course will be limited to "Probability Sampling" which is where probability is used to select the sample (in various ways)

*Simple Random Sampling (SRS)*

```
sample()
```
  
*Stratified Sampling*

```
dataset %>%
  group_by(strata_variable) %>%
  sample_n()
```

*Cluster Sampling*

```
sampling::cluster(dataset,
  cluster_var_name,
  number_to_select,
  method = "option")
```

### NHANES EDA {-}

```{r, eval = FALSE}
nhanes_combined <- read_csv("../data/nhanes_final.csv", guess_max = 7620)

nhanes_combined %>% glimpse()
```

Let's examine our newly constructed dataset with a mind toward EDA. As in the last chapter, it's a good idea to look at both numerical summary measures and visualizations. These help with understanding data and are a good way to find data cleaning steps you may have missed. The nhanes_combined dataset has been pre-loaded for you.

Say we have access to NHANES patients and want to conduct a study on the effect of being told by a physician to reduce calories/fat in their diet on weight. This is our treatment; we're pretending that instead of this being a question asked of the patient, we randomly had physicians counsel some patients on their nutrition. However, we suspect that there may be a difference in weight based on the gender of the patient - a blocking factor!

```{r, eval = FALSE}
# Fill in the dplyr code
nhanes_combined %>% 
  group_by(mcq365d) %>% 
  summarize(mean = mean(bmxwt, na.rm = TRUE))

# Fill in the ggplot2 code
nhanes_combined %>% 
  ggplot(aes(as.factor(mcq365d), bmxwt)) +
  geom_boxplot() +
  labs(x = "Treatment",
       y = "Weight")
```

### NHANES Data Cleaning {-}

During data cleaning, we discovered that no one under the age of 16 was given the treatment. Recall that we're pretending that the variable that indicates if a doctor has ever advised them to reduce fat or calories in their diet is purposeful nutrition counseling, our treatment. Let's only keep patients who are greater than 16 years old in the dataset.

You also may have noticed that the default settings in ggplot2 delete any observations with a missing dependent variable, in this case, body weight. One option for dealing with the missing weights, imputation, can be implemented using the simputation package. Imputation is a technique for dealing with missing values where you replace them either with a summary statistic, like mean or median, or use a model to predict a value to use.

We'll use `impute_median()`, which takes a dataset and the variable to impute or formula to impute by as arguments. For example, `impute_median(ToothGrowth, len ~ dose)` would fill in any missing values in the variable len with the median value for len by dose. So, if a guinea pig who received a dose of 2.0 had a missing value for the len variable, it would be filled in with the median len for those guinea pigs with a dose of 2.0.

```{r, eval = FALSE}
# Filter to keep only those 16+
# impute bmxwt by riagendr
# Recode mcq365d with recode() & examine with count()

trtmnt <- c(`1` = 1, `2` = 2, `9` = 2)

nhanes_final <- nhanes_combined %>%
  filter(ridageyr >= 16) %>%
  impute_median(bmxwt ~ riagendr) %>%
  mutate(mcq365d = recode(mcq365d, !!!trtmnt))

nhanes_final %>% count(mcq365d)
```

### Resampling NHANES data {-}

The NHANES data is collected on sampled units (people) specifically selected to represent the U.S. population. However, let's resample the `nhanes_final` dataset in different ways so we get a feel for the different sampling methods.

We can conduct a simple random sample using `sample_n()` from dplyr. It takes as input a dataset and an integer of number of rows to sample.

Stratified sampling can be done by combining `group_by()` and `sample_n()`. The function will sample n from each of the groups specified in the `group_by()`.

The `sampling` package's `cluster()` creates cluster samples. It takes in a dataset name, the variable in the set to be used as the cluster variable, passed as a vector with the name as a string (e.g. c("`variable`")), a number of clusters to select, and a method.

```{r, eval = FALSE}
# Use sample_n() to create nhanes_srs
# Randomly select 2500 observations
nhanes_srs <- nhanes_final %>% 
  sample_n(2500)

# Create nhanes_stratified with group_by() and sample_n()
# Randomly select 2000 observations from each gender
nhanes_stratified <- nhanes_final %>%
  group_by(riagendr) %>% 
	sample_n(2000)

# Confirm it pulled 2000/gender
nhanes_stratified %>%
  count(riagendr)

# Load sampling package and create nhanes_cluster with cluster()
# Use cluster() to divide nhanes_final by "indhhin2" into 6 clusters using the "srswor" method.
nhanes_cluster <- nhanes_final %>%
  cluster("indhhin2", 6, method = "srswor")
```

### Randomized Complete Block Designs (RCBD) {-}

*Block what you can. Randomized what you can't*

  - *R*andomized - the treatment is assigned randomly inside each block
  - *C*omplete - each treatment is used the same number of times in every block
  - *B* - experimental groups are blocked to be similar (e.g. by sex)
  - *D*esign
  
### Drawing RCBDs with Agricolae {-}

The `agricolae` package is very helpful when you want to "draw" out the design of an experiment for yourself using R. It can draw many different kinds of experiments, including a randomized complete block design. 

```{r, eval = FALSE}
# Create designs using ls()
designs <- ls("package:agricolae", pattern = "design")
designs

# Use str() to view design.rcbd's criteria
str(design.rcbd)

# Build treats and rep
treats <- LETTERS[1:5]
blocks <- 4

# Build my_design_rcbd and view the sketch
my_design_rcbd <- design.rcbd(treats, r = blocks, seed = 42)
my_design_rcbd$sketch
```

### NHANES RCBD {-}

Recall that our blocked experiment involved a treatment wherein the doctor asks the patient to reduce their fat or calories in their diet, and we're testing the effect this has on weight (`bmxwt`). We plan to block by gender, which in the NHANES dataset is stored as `riagendr`. Recall that blocking is done to create experimental groups that are as similar as possible. Blocking this experiment by gender means that if we observe an effect of the treatment on `bmxwt`, it's more likely that the effect was actually due to the treatment versus the individual's gender.

In your R code, you denote a blocked experiment by using a formula that looks like: `outcome ~ treatment + blocking_factor` in the appropriate modeling function.

```{r, eval = FALSE}
# Use aov() to create nhanes_rcbd
nhanes_rcbd <- aov(bmxwt ~ mcq365d + riagendr, data = nhanes_final)

# Check results of nhanes_rcbd with summary()
summary(nhanes_rcbd)

# Print mean weights by mcq365d and riagendr
nhanes_final %>% 
	group_by(mcq365d, riagendr) %>% 
	summarize(mean_wt = mean(bmxwt))
```

### RCBD Model Validation {-}

As we did in the last chapter (and when building any model!) it's a good idea to validate the results. We'll examine the Residuals vs. Fitted and Normal Q-Q plots, though now we'll only see a Constant Leverage plot in place of the other two. A good model has a Q-Q plot showing an approximately normal distribution and no clear patterns across blocks or treatments in the others.

We can also look at Interaction plots. We hope to see parallel lines, no matter which of the block or the treatment is on the x-axis. If they are, they satisfy a key assumption of the RCBD model called Additivity.

The initial diganostic plots show that this model is pretty good but not great - especially at the larger end of the data, the Q-Q plot shows the data might not be normal. The interaction plots show nearly parallel lines, so we can move forward with this model. 

```{r, eval = FALSE}
# Set up the 2x2 plotting grid and plot nhanes_rcbd
# For a 2x2 grid of plots:
par(mfrow=c(2, 2))

# Plot the nhanes_rcbd model object, being sure to set up a 2x2 grid of plots beforehand.
plot(nhanes_rcbd)

par(mfrow=c(2, 1))

# Run the code to view the interaction plot between the treatment and gender and observe if the lines are parallel.
with(nhanes_final, interaction.plot(mcq365d, riagendr, bmxwt))

#Run the code to view the interaction plot between gender and the treatment (it'll be a little different!) and observe if the lines are parallel.
with(nhanes_final, interaction.plot(riagendr, mcq365d, bmxwt))
```

### Balanced Incomplete Block Designs (BIBD) {-}

  - *B*alanced - each pair of treatments occur together in a block an equal number of times
  - *I*ncomplete - not every treatment will appear in every block
  - *B*lock - experimental groups are blocked to be similar
  - *D*esign

Is a BIBD possible?

$$ \lambda  =r \cdot \frac{k-1}{t-1}$$
where:

  - $t$ : number of treatments
  - $k$ : number of treatments per block
  - $r$ : number of replications

If $\lambda$ is a whole number, then a BIBD is possible, otherwise it is not.

### Drawing BIBDs with agricolae {-}

We can also use `agricolae` to draw BIBDs. `design.bib()` takes, at minimum, the treatments (`treats`), an integer `k` corresponding to the number of levels of the blocks, and a `seed` as inputs.

The main thing you should notice about a BIBD is that not every treatment will be used in each block (column) of the output.

From the video and the last exercise, however, you know that sometimes a BIBD isn't valid and that you have to do a little math to be sure your BIBD design is possible. `design.bib()` will return an error message letting you know if a design is not valid.

Let's draw a few BIBDs with `agricolae` so we can see the different warning messages and errors the package provides.

```{r, eval = FALSE}
# Create my_design_bibd_1 : INVALID
my_design_bibd_1  <- design.bib(LETTERS[1:3], k = 4, seed = 42)

# Create my_design_bibd_2 : INVALID
my_design_bibd_2  <- design.bib(LETTERS[1:8], k = 3, seed = 42)

# Create my_design_bibd_3
my_design_bibd_3  <- design.bib(LETTERS[1:4], k = 4, seed = 42)
my_design_bibd_3$sketch

```

### BIBD - cat's kidney function {-}

To be sure we truly understand what a BIBD looks like, let's build a dataset containing a BIBD from scratch.

Say we want to test the difference between four different wet foods in cats' diets on their kidney function. Cat food, however, is expensive, so we'll only test 3 foods per block to save some money. Our cats will be blocked by color of cat, as we aren't interested in that as part of our experiment. The outcome will be measured blood creatinine level, an indicator of kidney function and dysfunction in cats and humans alike.

```{r, eval = FALSE}
lambda <- function(t, k, r){
  return((r*(k-1)) / (t-1))
}
```

```{r, eval = FALSE}
# Calculate lambda
lambda(4, 3, 3)

# Build the data.frame
creatinine <- c(1.98, 1.97, 2.35, 2.09, 1.87, 1.95, 2.08, 2.01, 1.84, 2.06, 1.97, 2.22)
food <- as.factor(c("A", "C", "D", "A", "B", "C", "B", "C", "D", "A", "B", "D"))
color <- as.factor(rep(c("Black", "White", "Orange", "Spotted"), each = 3))
cat_experiment <- as.data.frame(cbind(creatinine, food, color))

# Create cat_model and examine with summary()
cat_model <- aov(creatinine ~ food + color, data = cat_experiment)
summary(cat_model)
```

### NHANES BIBD {-}

Let's jump back into the NHANES data and pretend we have access to NHANES patients ages 18-45. Blocking the adults by race, stored in NHANES as `ridreth1`, we prescribe to our groups either no particular upper body weightlifting regimen, a weightlifting regimen, or a weightlifting regimen plus a prescribed daily vitamin supplement. This information is stored in a variable called `weightlift_treat`.

Those funding the study decide they want it to be a BIBD where only 2 treatments appear in each block. The outcome, arm circumference, is stored as `bmxarmc`.

```r
## weightlift_treat is missing from my data but its in the DC data

# Calculate lambda
lambda(3, 2, 2)

# Create weightlift_model & examine results
weightlift_model <- aov(bmxarmc ~ weightlift_treat + ridreth1, data = nhanes_final)
summary(weightlift_model)
```

## Latin Squares, Graeco-Latin Squares, & Factorial experiments

### Latin squares {-}

