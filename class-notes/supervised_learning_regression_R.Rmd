# Supervised Learning in R: Regression

```{r}
library(tidyverse)
```

```{r}
bikes <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/6feade63b6c9f866433b94752677845ed1b22914/Bikes.RData")
bp <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/9210064536fb0cb88a265c748f65dfc4761b2292/bloodpressure.rds")
cricket <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/a2b224940b225fc552d243287b564ca126aa71c5/cricket.rds")
houses <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/6f144237ef9d7da94b2c84aa8eccc519bae4b300/houseprice.rds")
income <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/a3b41b96dcb08ad84c82ca6b45f27a36832700e5/Income.RData")
mpg <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/a3ea08fe688ee3afdc142a2641a0e035b09ef9e0/Mpg.RData")
soybean <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/1702e2cf57da42eb449515ec71aacba41bf3a6db/Soybean.RData")
unemployment <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/e05279335f3b0e5a768f1fcd9b55d047a9f5faeb/unemployment.rds")
sparrow <- rio::import("https://assets.datacamp.com/production/repositories/894/datasets/30d1dbf4d0788fdd293f8a2b4ac5614f109eb1c1/sparrow.rds")
```

## What is Regression?

## Training and Evaluating Regression Models

## Issues to Consider

## Dealing with Non-Linear Responses