# Working with Web Data in R

```{r, eval = FALSE}
library(tidyverse)
library(pageviews)
library(httr)
library(jsonlite)
library(rlist)
library(xml2)
library(rvest)
```


## Downloading Files and Using API Clients

### Downloading files and reading them into R {-}

```{r, eval = FALSE}
# Here are the URLs! As you can see they're just normal strings
csv_url <- "http://s3.amazonaws.com/assets.datacamp.com/production/course_1561/datasets/chickwts.csv"
tsv_url <- "http://s3.amazonaws.com/assets.datacamp.com/production/course_3026/datasets/tsv_data.tsv"

# Read a file in from the CSV URL and assign it to csv_data
csv_data <- read.csv(csv_url)

# Read a file in from the TSV URL and assign it to tsv_data
tsv_data <- read.delim(tsv_url)

# Examine the objects with head()
csv_data %>% head()
tsv_data %>% head()
```

Sometimes just reading the file in from the web is enough, but often you'll want to store it locally so that you can refer back to it. This also lets you avoid having to spend the start of every analysis session twiddling your thumbs while particularly large files download.

Helpfully, R has `download.file()`, a function that lets you do just that: download a file to a location of your choice on your computer. It takes two arguments; url, indicating the URL to read from, and destfile, the destination to write the downloaded file to. In this case, we've pre-defined the URL - once again, it's `csv_url`.

```{r, eval = FALSE}
# Download the file with download.file()
download.file(url = csv_url, destfile = "../data/feed_data.csv")

# Read it in with read.csv()
csv_data <- read_csv("../data/feed_data.csv")
```

### Understanding Application Programming Interfaces (APIs) {-}

So we know that APIs are server components to make it easy for your code to interact with a service and get data from it. We also know that R features many "clients" - packages that wrap around connections to APIs so you don't have to worry about the details.

Let's look at a really simple API client - the `pageviews` package, which acts as a client to Wikipedia's API of pageview data. As with other R API clients, it's formatted as a package, and lives on CRAN - the central repository of R packages. The goal here is just to show how simple clients are to use: they look just like other R code, because they are just like other R code.

```{r, eval = FALSE}
# Get the pageviews for "Hadley Wickham"
hadley_pageviews <- article_pageviews(project = "en.wikipedia", "Hadley Wickham")

# Examine the resulting object
str(hadley_pageviews)
```

### Access tokens and APIs {-}

It's common for APIs to require access tokens - unique keys that verify you're authorised to use a service. They're usually pretty easy to use with an API client.

## Using httr to interact with APIs directly

### GET and POST requests in theory {-}

HTTP requests (most common requests)
  
  - GET: "get me something"
  - POST: "have something of mine"
  
### GET requests in practice {-}

To start with you're going to make a GET request. As discussed in the video, this is a request that asks the server to give you a particular piece of data or content (usually specified in the URL). These make up the majority of the requests you'll make in a data science context, since most of the time you'll be getting data from servers, not giving it to them.

To do this you'll use the `httr` package, written by Hadley Wickham (of course), which makes HTTP requests extremely easy. You're going to make a very simple GET request, and then inspect the output to see what it looks like.

```{r, eval = FALSE}
# Make a GET request to http://httpbin.org/get
get_result <- httr::GET("http://httpbin.org/get")

# Print it to inspect it
print(get_result)
```

### POST requests in practice {-}

Next we'll look at POST requests, also made through httr, with the function (you've guessed it) `POST()`. Rather than asking the server to give you something, as in GET requests, a POST request asks the server to accept something from you. They're commonly used for things like file upload, or authentication. As a result of their use for uploading things, `POST()` accepts not just a `url` but also a `body` argument containing whatever you want to give to the server.

You'll make a very simple POST request, just uploading a piece of text, and then inspect the output to see what it looks like.

```{r, eval = FALSE}
# Make a POST request to http://httpbin.org/post with the body "this is a test"
post_result <- POST("http://httpbin.org/post", body = "this is a test")

# Print it to inspect it
post_result
```

### Extracting the response {-}

Making requests is all well and good, but it's also not why you're here. What we really want to do is get the data the server sent back, which can be done with `httr`'s `content()` function. You pass it an object returned from a `GET` (or `POST`, or `DELETE`, or...) call, and it spits out whatever the server actually sent in an R-compatible structure.

We're going to demonstrate that now, using a slightly more complicated URL than before - in fact, using a URL from the Wikimedia pageviews system you dealt with through the `pageviews` package, which is stored as `url.` Without looking too much at the structure for the time being (we'll get to that later) this request asks for the number of pageviews to the English-language Wikipedia's "Hadley Wickham" article on 1 and 2 January 2017.

```{r, eval = FALSE}
url <- "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia.org/all-access/all-agents/Hadley_Wickham/daily/20170101/20170102"

# Make a GET request to url and save the results
pageview_response <- GET(url)

# Call content() to retrieve the data the server sent back
pageview_data <- content(pageview_response)

# Examine the results with str()
str(pageview_data)
```

### Graceful httr {-}

  - Error handling
  - Taking different user inputs
  
General rules of thumb for website error codes:

  - 2XX/3XX - Everything is okay
  - 4XX - Your code is broken
  - 5XX - Their code is broken

API urls tend to have one of two forms:

  1. Directory-based URLS
    - slash-separated, like directories
    - Very common with more modern apis
    - Example URL: `https://fakeurl.com/api/peaches/thursday`
    - Can use paste to build URLS
  2. Parameter-based URLS
    - Uses URL parameters (e.g. `a=1&b=2`)
    - Example URL: `https://fakeurl.com/api.php?fruit=peaches&day=thursday`
    - Use `GET()` to construct the URL with the query argument
    
### Handling http failures {-}

As mentioned, HTTP calls can go wrong. Handling that can be done with httr's `http_error()` function, which identifies whether a server response contains an error.

If the response does contain an error, calling `http_error()` over the response will produce TRUE; otherwise, FALSE. You can use this for really fine-grained control over results. For example, you could check whether the request contained an error, and (if so) issue a warning and re-try the request.

For now we'll try something a bit simpler - issuing a warning that something went wrong if `http_error()` returns TRUE, and printing the content if it doesn't.

```{r, eval = FALSE}
fake_url <- "http://google.com/fakepagethatdoesnotexist"

# Make the GET request
request_result <- GET(fake_url)

# Check request_result
if(http_error(request_result)){
  warning("The request failed")
} else {
	content(request_result)
}
```

### Constructing queries (PI) {-}

As briefly discussed in the previous video, the actual API query (which tells the API what you want to do) tends to be in one of the two forms. The first is directory-based, where values are separated by / marks within the URL. The second is parameter-based, where all the values exist at the end of the URL and take the form of key=value.

Constructing directory-based URLs can be done via paste(), which takes an unlimited number of strings, along with a separator, as sep. So to construct http://swapi.co/api/vehicles/12 we'd call:

`paste("http://swapi.co", "api", "vehicles", "12", sep = "/")`

Let's do that now! We'll cover parameter-based URLs later. In the mean time we can play with SWAPI, mentioned above, which is an API chock full of star wars data. This time, rather than a vehicle, we'll look for a person.

```{r, eval = FALSE}
# Construct a directory-based API URL to `http://swapi.co/api`,
# looking for person `1` in `people`
directory_url <- paste("http://swapi.co/api", "people", "1", sep = "/")

# Make a GET call with it
result <- GET(directory_url)
```

### Constructing queries (PII) {-}

As mentioned (albeit briefly) in the last exercise, there are also parameter based URLs, where all the query values exist at the end of the URL and take the form of key=value - they look something like http://fakeurl.com/foo.php?country=spain&food=goulash

Constructing parameter-based URLs can also be done with `paste()`, but the easiest way to do it is with `GET()` and `POST()` themselves, which accept a query argument consisting of a list of keys and values. So, to continue with the food-based examples, we could construct `fakeurl.com/api.php?fruit=peaches&day=thursday` with:

`GET("fakeurl.com/api.php", query = list(fruit = "peaches", day = "thursday"))`

In this exercise you'll construct a call to `https://httpbin.org/get?nationality=americans&country=antigua`.

```{r, eval = FALSE}
# Create list with nationality and country elements
query_params <- list(
  nationality = "americans", 
  country = "antigua"
)
    
# Make parameter-based call to httpbin, with query_params
parameter_response <- GET("https://httpbin.org/get", query = query_params)

# Print parameter_response
parameter_response
```

### Respectful API Usage {-}

User Agents 

  - bits of text that ID your browser/software
  - Give the server some idea of what you're trying to do
  - You can set one with your requests with `user_agent()`
  - Add an email address so they can contact you

Rate Limiting

  - Too many requests makes for a sad server
  - Deliberately slows down your code to keep under a desired "rate"
  
### Using user agents {-}

As discussed in the video, informative user-agents are a good way of being respectful of the developers running the API you're interacting with. They make it easy for them to contact you in the event something goes wrong. I always try to include:

  - My email address
  - A URL for the project the code is a part of, if it's got a URL.

Building user agents is done by passing a call to `user_agent()` into the `GET()` or `POST()` request; something like:

`GET("http://url.goes.here/", user_agent("somefakeemail@domain.com http://project.website"))`

In the event you don't have a website, a short one-sentence description of what the project is about serves pretty well.

```{r, eval = FALSE}
# Do not change the url
url <- "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/Aaron_Halfaker/daily/2015100100/2015103100"

# Add the email address and the test sentence inside user_agent()
server_response <- GET(url, user_agent("my@email.address this is a test"))
```

### Rate-limiting {-}

The next stage of respectful API usage is rate-limiting: making sure you only make a certain number of requests to the server in a given time period. Your limit will vary from server to server, but the implementation is always pretty much the same and involves a call to` Sys.sleep()`. This function takes one argument, a number, which represents the number of seconds to "sleep" (pause) the R session for. So if you call `Sys.sleep(15)`, it'll pause for 15 seconds before allowing further code to run.

As you can imagine, this is really useful for rate-limiting. If you are only allowed 4 requests a minute? No problem! Just pause for 15 seconds between each request and you're guaranteed to never exceed it. Let's demonstrate now by putting together a little loop that sends multiple requests on a 5-second time delay. We'll use `httpbin.org` 's APIs, which allow you to test different HTTP libraries.

```{r, eval = FALSE}
# Construct a vector of 2 URLs
urls <- c("http://httpbin.org/status/404", "http://httpbin.org/status/301")

for(url in urls){
    # Send a GET request to url
    result <- GET(url)
    
    # Delay for 5 seconds between requests
    Sys.sleep(5)
}
```

### Tying it all together {-}

Using everything that you learned in the chapter, let's make a simple replica of one of the `pageviews` functions - building queries, sending `GET` requests (with an appropriate user agent) and handling the output in a fault-tolerant way. You'll build this function up piece by piece in this exercise.

To output an error, you will use the function `stop()`, which takes a string as an argument, stops the execution of the program, and outputs the string as an error. You can try it right now by running `stop("This is an error")`.


```{r, eval = FALSE}
get_pageviews <- function(article_title){
  url <- paste(
    "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents", 
    article_title, 
    "daily/2015100100/2015103100", 
    sep = "/"
  ) 
  # Get the webpage  
  response <- GET(url, config = user_agent("my@email.com this is a test"))
  
    # Is there an HTTP error?
  if(http_error(response)){ 
    # Throw an R error
     stop("the request failed") 
  }
  
  content(response)
}
```

## Handling JSON and XML

NOTE:: THIS CODE  FOR THIS SECTION WILL NOT WORK! IT LOADS A LOCAL FILE ON DC

`fromJSON` Useful arguments

   - `simplifyVector = TRUE` converts arrays of primitives to vectors
   - `simplifyDataFrame = TRUE` converts arrays of objects to data frames

### JSON {-}

  - JavaScript Object Notation
  - plain text format
  - 2 structures:
    - Objects: `{"title" : "A New Hope", "year" : "1997"}`
    - Arrays: `[1997, 1980]`
  - Values: strings, numbers, booleans, nulls, or a nested object or array

### Parsing JSON {-}

While JSON is a useful format for sharing data, your first step will often be to parse it into an R object, so you can manipulate it with R.

The `content()` function in httr retrieves the content from a request. It takes an as argument that specifies the type of output to return. You've already seen that `as = "text"` will return the content as a character string which is useful for checking the content is as you expect.

If you don't specify as, the default `as = "parsed"` is used. In this case the type of `content()` will be guessed based on the header and `content()` will choose an appropriate parsing function. For JSON this function is `fromJSON()` from the jsonlite package. If you know your response is JSON, you may want to use `fromJSON()` directly.

To practice, you'll retrieve some revision history from the Wikipedia API, check it is JSON, then parse it into a list two ways.


Get the revision history for the Wikipedia article for "Hadley Wickham", by calling r`ev_history("Hadley Wickham")` (a function we have written for you), store it in `resp_json`.

```{r, eval = FALSE}
rev_history <- function(title, format = "json"){
  if (title != "Hadley Wickham") {
    stop('rev_history() only works for `title = "Hadley Wickham"`')
  }
  
  if (format == "json"){
    resp <- readRDS("had_rev_json.rds")
  } else if (format == "xml"){
    resp <- readRDS("had_rev_xml.rds")
  } else {
    stop('Invalid format supplied, try "json" or "xml"')
  }
  resp  
}
```

```{r, eval = FALSE}
# Get revision history for "Hadley Wickham"
resp_json <- rev_history("Hadley Wickham")

# Check http_type() of resp_json
http_type(resp_json) # "application/json"

# Examine returned text with content()
content(resp_json, as = "text")

# Parse response with content()
content(resp_json, as = "parsed")

# Parse returned text with fromJSON()
library(jsonlite)
fromJSON(content(resp_json, as = "text"))
```

Manipulating parsed JSON

As you saw in the video, the output from parsing JSON is a list. One way to extract relevant data from that list is to use a package specifically designed for manipulating lists, `rlist`.

`rlist` provides two particularly useful functions for selecting and combining elements from a list: `list.select()` and `list.stack()`. `list.select()` extracts sub-elements by name from each element in a list. For example using the parsed movies data from the video (`movies_list`), we might ask for the title and year elements from each element:

`list.select(movies_list, title, year)`

The result is still a list, that is where list.stack() comes in. It will stack the elements of a list into a data frame.

```
list.stack(
    list.select(movies_list, title, year)
)
```

In this exercise you'll use these `rlist` functions to create a data frame with the user and timestamp for each revision.

```{r, eval = FALSE}
# Examine output of this code
str(content(resp_json), max.level = 4)

# Store revision list
revs <- content(resp_json)$query$pages$`41916270`$revisions

# Extract the user element
user_time <- list.select(revs, user, timestamp)

# Print user_time
user_time

# Stack to turn into a data frame
list.stack(user_time)
```

### Reformatting JSON {-}

Of course you don't have to use `rlist`. You can achieve the same thing by using functions from base R or the tidyverse. In this exercise you'll repeat the task of extracting the username and timestamp using the `dplyr` package which is part of the tidyverse.

Conceptually, you'll take the list of revisions, stack them into a data frame, then pull out the relevant columns.

dplyr's `bind_rows()` function takes a list and turns it into a data frame. Then you can use `select()` to extract the relevant columns. And of course if we can make use of the `%>%` (pipe) operator to chain them all together.

Try it!

```{r, eval = FALSE}
# Pull out revision list
revs <- content(resp_json)$query$pages$`41916270`$revisions

# Extract user and timestamp
revs %>%
  bind_rows() %>%           
  select(user, timestamp)
```

### XML Structure {-}

XML is dividied into:

  - Markup
    - Describes the structure of the data
    - Mostly in form of tags `<tagname></tagname>`
    - Markup can also exist as attributes inside a tag in the form of name value pairs `<tagname att = "ex">`
    - Conventional metadata is stored as attributes, and whereas data-data is stored as content.
  - Content
    - The data

A single `element` in XML is all content between tag pairs. Children elements can be contained with parent elements.

### Examining XML documents {-}

Just like JSON, you should first verify the response is indeed XML with `http_type()` and by examining the result of `content(r, as = "text")`. Then you can turn the response into an XML document object with `read_xml()`.

One benefit of using the XML document object is the available functions that help you explore and manipulate the document. For example `xml_structure()` will print a representation of the XML document that emphasizes the hierarchical structure by displaying the elements without the data.

In this exercise you'll grab the same revision history you've been working with as XML, and take a look at it with `xml_structure()`.

```{r, eval = FALSE}
# Get XML revision history
resp_xml <- rev_history("Hadley Wickham", format = "xml")

# Check response is XML 
http_type(resp_xml)

# Examine returned text with content()
rev_text <- content(resp_xml, as = "text")
rev_text

# Turn rev_text into an XML document
rev_xml <- read_xml(rev_text)

# Examine the structure of rev_xml
xml_structure(rev_xml)
```

### XPATHs {-}

XPath - a language for specifying nodes in an XML document

  - Specify locations of nodes like file paths (sep with `/`)
    - e.g.  `xml_find_all(xml_obj, "/node_name/path1/path2")`
  - `//` a node at any level below
    - e.g. `xml_find_all(xml_obj, "//node_name")` finds all `node_name` tags any where in the xml
  - `@` to extract attributes
    - e.g. `xml_find_all(xml_obj, "//node_name/@att")` finds all `node_name` tags any where in the xml
    - also can use `xml_attr()` and `xml_attrs()`
    
  Other useful functions
    - `xml_double()`
    - `xml_integar()`
    - `as_list()`

### Extracting XML data {-}

XPATHs are designed to specifying nodes in an XML document. Remember `/node_name` specifies nodes at the current level that have the tag `node_name`, where as `//node_name` specifies nodes at any level below the current level that have the tag `node_name.`

`xml2` provides the function `xml_find_all()` to extract nodes that match a given XPATH. For example, `xml_find_all(rev_xml, "/api")` will find all the nodes at the top level of the `rev_xml` document that have the tag `api`. Try running that in the console. You'll get a nodeset of one node because there is only one node that satisfies that XPATH.

The object returned from `xml_find_all()` is a nodeset (think of it like a list of nodes). To actually get data out of the nodes in the nodeset, you'll have to explicitly ask for it with `xml_text()` (or `xml_double()` or `xml_integer()`).

Use what you know about the location of the revisions data in the returned XML document extract just the content of the revision.

```{r, eval = FALSE}
# Find all nodes using XPATH "/api/query/pages/page/revisions/rev"
xml_find_all(rev_xml, "/api/query/pages/page/revisions/rev")

# Find all rev nodes anywhere in document
rev_nodes <- xml_find_all(rev_xml, "//rev")

# Use xml_text() to get text from rev_nodes
xml_text(rev_nodes)
```


### Extracting XML attributes {-}

Not all the useful data will be in the content of a node, some might also be in the attributes of a node. To extract attributes from a nodeset, `xml2` provides `xml_attrs()` and `xml_attr()`.

`xml_attrs()` takes a nodeset and returns all of the attributes for every node in the nodeset. `xml_attr()` takes a nodeset and an additional argument `attr` to extract a single named argument from each node in the nodeset.

In this exercise you'll grab the user and anon attributes for each revision. You'll see `xml_find_first()` in the sample code. It works just like `xml_find_all()` but it only extracts the first node it finds.

```{r, eval = FALSE}
# All rev nodes
rev_nodes <- xml_find_all(rev_xml, "//rev")

# The first rev node
first_rev_node <- xml_find_first(rev_xml, "//rev")

# Find all attributes with xml_attrs()
xml_attrs(first_rev_node)

# Find user attribute with xml_attr()
xml_attr(first_rev_node, "user")

# Find user attribute for all rev nodes
xml_attr(rev_nodes, "user")

# Find anon attribute for all rev nodes
xml_attr(rev_nodes, "anon")
```

### Wrapup: returning nice API output {-}

How might all this work together? A useful API function will retrieve results from an API and return them in a useful form. In Chapter 2, you finished up by writing a function that retrieves data from an API that relied on `content()` to convert it to a useful form. To write a more robust API function you shouldn't rely on `content()` but instead parse the data yourself.

To finish up this chapter you'll do exactly that: write `get_revision_history()` which retrieves the XML data for the revision history of page on Wikipedia, parses it, and returns it in a nice data frame.

So that you can focus on the parts of the function that parse the return object, you'll see your function calls `rev_history(`) to get the response from the API. You can assume this function returns the raw response and follows the best practices you learnt in Chapter 2, like using a user agent, and checking the response status.


```{r, eval = FALSE}
get_revision_history <- function(article_title){
  # Get raw revision response
  rev_resp <- rev_history(article_title, format = "xml")

  # Turn the content() of rev_resp into XML
  rev_xml <- read_xml(content(rev_resp, "text"))

  # check out xml structure
  xml_structre(rev_xml)
  
  # Find revision nodes
  rev_nodes <- xml_find_all(rev_xml, "//rev")

  # Parse out usernames
  user <- xml_attr(rev_nodes, "user")

  # Parse out timestamps
  timestamp <- readr::parse_datetime(xml_attr(rev_nodes, "timestamp"))

  # Parse out content
  content <- xml_text(rev_nodes)

  # Return data frame
  data.frame(user = user,
    timestamp = timestamp,
    content = substr(content, 1, 40))
}

# Call function for "Hadley Wickham"
get_revision_history("Hadley Wickham")
```

## Web scraping with XPATHS

### Web scraping 101 {-}

This course has pretty selected the elements of interest on the web page. On your own projects, you have to find the right elements yourself. There are browser extensions called `Selectors` to make this easier. Check it out after this course.

`rvest::read_html(url)` returns an XML doc. `rvest::html_node() is for extracting contents with XPATHs`

### Reading HTML {-}

The first step with web scraping is actually reading the HTML in. This can be done with a function from `xml2`, which is imported by `rvest` - `read_html()`. This accepts a single URL, and returns a big blob of XML that we can use further on.

We're going to experiment with that by grabbing Hadley Wickham's wikipedia page, with `rvest`, and then printing it just to see what the structure looks like.

```{r, eval = FALSE}
# Hadley Wickham's Wikipedia page
test_url <- "https://en.wikipedia.org/wiki/Hadley_Wickham"

# Read the URL stored as "test_url" with read_html()
test_xml <- read_html(test_url)

# Print test_xml
test_xml
```

### Extracting nodes by XPATH {-}

Now you've got a HTML page read into R. Great! But how do you get individual, identifiable pieces of it?

The answer is to use `html_node()`, which extracts individual chunks of HTML from a HTML document. There are a couple of ways of identifying and filtering nodes, and for now we're going to use XPATHs: unique identifiers for individual pieces of a HTML document.

These can be retrieved using a browser gadget we'll talk about later - in the meanwhile the XPATH for the information box in the page you just downloaded is stored as test_node_xpath. We're going to retrieve the box from the HTML doc with `html_node()`, using `test_node_xpath` as the `xpath` argument.

```{r, eval = FALSE}
test_node_xpath <-  "//*[contains(concat( \" \", @class, \" \" ), concat( \" \", \"vcard\", \" \" ))]"

# Use html_node() to grab the node with the XPATH stored as `test_node_xpath`
table_element <- html_node(x = test_xml, xpath = test_node_xpath)

# Print the first element of the result
table_element[1]
```

### HTML structure {-}

Common tags/attributes:

  - Links: `<a href = "link.com"> CLICK ME </a>`

rvest contains the following:

  - `html_text()` : get text contents
  - `html_attr()` : get specific attribute
  - `html_name()` : get tag name


### Extracting names {-}

The first thing we'll grab is a name, from the first element of the previously extracted table (now stored as `table_element`). We can do this with `html_name()`. As you may recall from when you printed it, the element has the tag `<table>...</table>`, so we'd expect the name to be, well, `table.`

```{r, eval = FALSE}
# Extract the name of table_element
element_name <- html_name(table_element)

# Print the name
element_name
```

### Extracting values {-}

Just knowing the type of HTML object a node is isn't much use, though (although it can be very helpful). What we really want is to extract the actual text stored within the value.

We can do that with (shocker) `html_text()`, another convenient `rvest` function that accepts a node and passes back the text inside it. For this we'll want a node within the extracted element - specifically, the one containing the page title. The xpath value for that node is stored as `second_xpath_val.`

Using this xpath value, extract the node within `table_element` that we want, and then use `html_text` to extract the text, before printing it.

```{r, eval = FALSE}
second_xpath_val <- "//*[contains(concat( \" \", @class, \" \" ), concat( \" \", \"fn\", \" \" ))]"

# Extract the element of table_element referred to by second_xpath_val and store it as page_name
page_name <- html_node(x = table_element, xpath = second_xpath_val)

# Extract the text from page_name
page_title <- html_text(page_name)

# Print page_title
page_title
```

### Reformatting Data {-}

  - HTML tables are dedicated structures: `<table> ... </table>`
  - They can be turned into data.frames with `html_table()`
  - Use `colnames(table) <- c("name", "second_name")` to rename the columns

However, most HTML isn't stored as tables, but can still be turned into data.frames.

### Extracting tables {-}

The data from Wikipedia that we've been playing around with can be extracted bit by bit and cleaned up manually, but since it's a table, we have an easier way of turning it into an R object. `rvest` contains the function `html_table()` which, as the name suggests, extracts tables. It accepts a node containing a table object, and outputs a data frame.

Let's use it now: take the table we've extracted, and turn it into a data frame.

```{r, eval = FALSE}
# Turn table_element into a data frame and assign it to wiki_table
wiki_table <- html_table(table_element)

# Print wiki_table
wiki_table
```

### Cleaning a data frame {-}

In the last exercise, we looked at extracting tables with `html_table()`. The resulting data frame was pretty clean, but had two problems - first, the column names weren't descriptive, and second, there was an empty row.

In this exercise we're going to look at fixing both of those problems. First, column names. Column names can be cleaned up with the `colnames()` function. You call it on the object you want to rename, and then assign to that call a vector of new names.

The missing row, meanwhile, can be removed with the `subset()` function. `subset` takes an object, and a condition. For example, if you have a data frame df containing a column x, you could run

`subset(df, !x == "")`

to remove all rows from df consisting of empty strings (`""`) in the column `x`.

```{r, eval = FALSE}
# Rename the columns of wiki_table
colnames(wiki_table) <- c("key", "value")

# Remove the empty row from wiki_table
cleaned_table <- subset(wiki_table, key != "")

# Print cleaned_table
cleaned_table
```

## CSS Web Scraping and Final Case Study

CSS - Cascading Style Sheets

For HTML, css allows us to define style conventions (font, size, color, etc.) to all classes of a given type to avoid duplicative code and make it easier to update a website's look and feel.

### Using CSS to scrape nodes {-}

As mentioned in the video, CSS is a way to add design information to HTML, that instructs the browser on how to display the content. You can leverage these design instructions to identify content on the page.

You've already used `html_node()`, but it's more common with CSS selectors to use `html_nodes()` since you'll often want more than one node returned. Both functions allow you to specify a css argument to use a CSS selector, instead of specifying the xpath argument.

```{r, eval = FALSE}
# Select the table elements
html_nodes(test_xml, css = "table")

# Select elements with class = "infobox"
html_nodes(test_xml, css = ".infobox")

# Select elements with id = "firstHeading"
html_nodes(test_xml, css = "#firstHeading")
```

### Scraping names {-}

You might have noticed in the previous exercise, to select elements with a certain class, you add a `.` in front of the class name. If you need to select an element based on its id, you add a `#` in front of the id name.

For example if this element was inside your HTML document:

```
<h1 class = "heading" id = "intro">
  Introduction
</h1>
```

You could select it by its class using the CSS selector `".heading"`, or by its id using the CSS selector `"#intro"`.

Once you've selected an element with a CSS selector, you can get the element tag name just like you did with XPATH selectors, with `html_name()`.

```{r, eval = FALSE}
# Extract element with class infobox
infobox_element <- html_nodes(test_xml, css = ".infobox")

# Get tag name of infobox_element
element_name <- html_name(infobox_element)

# Print element_name
element_name
```

### Scraping text {-}

Of course you can get the contents of a node extracted using a CSS selector too, with `html_text()`.

Can you put the pieces together to get the page title like you did in Chapter 4?

```{r, eval = FALSE}
# Extract element with class fn
page_name <- html_node(x = infobox_element, css = ".fn")

# Get contents of page_name
page_title <- html_text(page_name)

# Print page_title
page_title
```

### Intro to case study {-}

### API calls {-}

Your first step is to use the Wikipedia API to get the page contents for a specific page. We'll continue to work with the Hadley Wickham page, but as your last exercise, you'll make it more general.

```{r, eval = FALSE}
# The API url
base_url <- "https://en.wikipedia.org/w/api.php"

# Set query parameters
query_params <- list(
  action = "parse", 
  page = "Hadley Wickham",
  format = "xml"
)

# Get data from API
resp <- GET(url = base_url, query = query_params)
    
# Parse response
resp_xml <- content(resp)
```

### Extracting information {-}

Now we have a response from the API, we need to extract the HTML for the page from it. It turns out the HTML is stored in the contents of the XML response.

```{r, eval = FALSE}
# Read page contents as HTML
page_html <- read_html(xml_text(resp_xml))

# Extract infobox element
infobox_element <- html_node(page_html, css = ".infobox")

# Extract page name element from infobox
page_name <- html_node(infobox_element, css = ".fn")

# Extract page name as text
page_title <- html_text(page_name)
```

### Normalising information {-}

Now it's time to put together the information in a nice format. You've already seen you can use `html_table()` to parse the infobox into a data frame. But one piece of important information is missing from that table: who the information is about!

```{r, eval = FALSE}
# Create a dataframe for full name
name_df <- data.frame(key = "Full name", value = page_title)

# Combine name_df with cleaned_table
wiki_table2 <- rbind(name_df, cleaned_table)

# Print wiki_table
wiki_table2
```

### Reproducibility {-}

```{r, eval = FALSE}
get_infobox <- function(title){
  base_url <- "https://en.wikipedia.org/w/api.php"
  
  # Change "Hadley Wickham" to title
  query_params <- list(action = "parse", 
    page = title, 
    format = "xml")
  
  resp <- GET(url = base_url, query = query_params)
  resp_xml <- content(resp)
  
  page_html <- read_html(xml_text(resp_xml))
  infobox_element <- html_node(x = page_html, css =".infobox")
  page_name <- html_node(x = infobox_element, css = ".fn")
  page_title <- html_text(page_name)
  
  wiki_table <- html_table(infobox_element)
  colnames(wiki_table) <- c("key", "value")
  cleaned_table <- subset(wiki_table, !wiki_table$key == "")
  name_df <- data.frame(key = "Full name", value = page_title)
  wiki_table <- rbind(name_df, cleaned_table)
  
  wiki_table
}
```


```{r, eval = FALSE}
# Test get_infobox with "Hadley Wickham"
get_infobox(title = "Hadley Wickham")

# Try get_infobox with "Ross Ihaka"
get_infobox(title = "Ross Ihaka")

# Try get_infobox with "Grace Hopper"
get_infobox(title = "Grace Hopper")
```

